# Exit-Ticket

Exit Ticket is a preset for the Moodle activity database.

## Demo

https://fdagner.de/moodle/mod/data/view.php?d=9

## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

## Language Support

The preset is available in German. 

## Description

Use this technique to show you what students are thinking and what they have learned at the end of a lesson.

## License

https://creativecommons.org/publicdomain/zero/1.0/deed.de

## Screenshots

<img width="400" alt="single view" src="/screenshots/einzelansicht.png">

<img width="400" alt="list view" src="/screenshots/neuereintrag.png">


